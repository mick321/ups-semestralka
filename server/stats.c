#include "stats.h"
#include <stdio.h>
#include <pthread.h>

pthread_mutex_t stats_mutex;

unsigned int allMessagesInCount = 0;
unsigned int allMessagesInBytes = 0;
unsigned int allMessagesOutCount = 0;
unsigned int allMessagesOutBytes = 0;

unsigned int allMessagesInvalid = 0;

unsigned int connectionCount = 0;

unsigned int gamesCount = 0;
unsigned int gamesFinished = 0;

unsigned int playerLeftCount = 0;
unsigned int playerReconnectCount = 0;

FILE* trace_file;

/*
  pomocna funkce pro tisknuti casovych razitek 
*/
int print_timestamp()
{
    time_t rawtime;
    struct tm* timeinfo;
    char buffer[80];

    time(&rawtime);
    timeinfo = localtime(&rawtime);

    strftime(buffer, 80, "%c", timeinfo);
    fprintf(trace_file, "[%s]\n", buffer);

    return 0;
}

/*
  inicializace
*/
int stats_init()
{
    allMessagesInCount = 0;
    allMessagesInBytes = 0;
    allMessagesOutCount = 0;
    allMessagesOutBytes = 0;

    allMessagesInvalid = 0;

    connectionCount = 0;

    gamesCount = 0;
    gamesFinished = 0;

    playerLeftCount = 0;
    playerReconnectCount = 0;
    
    pthread_mutex_init(&stats_mutex, NULL);

    trace_file = fopen("trace.log", "a");

    return 0;
}

/*
  vytisknuti statistik
*/
int stats_print()
{
    printf("\n----\n\n");
    printf("STATISTIKY:\n");
    printf("Prichozi zpravy: %d (%d bajtu)\n", allMessagesInCount, allMessagesInBytes);
    printf("Odchozi zpravy: %d (%d bajtu)\n", allMessagesOutCount, allMessagesOutBytes);
    printf("Celkem: %d (%d bajtu)\n\n", 
       allMessagesInCount + allMessagesOutCount, allMessagesInBytes + allMessagesOutBytes);
    printf("Neplatne prichozi zpravy: %d\n\n", allMessagesInvalid);

    printf("Pocet pokusu o pripojeni k serveru: %d\n", connectionCount);
    printf("Pocet spustenych her: %d (dohrano %d)\n", gamesCount, gamesFinished);
    printf("Pocet ztracenych spojeni: %d (zpet se pripojilo %d)\n", playerLeftCount, playerReconnectCount);

    printf("\n----\n\n");

    fclose(trace_file);

    return 0;
}

/* zapocteni prichozi zpravy */
int stats_message_in(int bytes)
{
  pthread_mutex_lock(&stats_mutex);
  allMessagesInCount++;
  allMessagesInBytes += bytes;

  print_timestamp();
  fprintf(trace_file, "Prichozi zprava - %d bajtu\n", bytes);

  pthread_mutex_unlock(&stats_mutex);
  return 0;
}

/* zapocteni odchozi zpravy */
int stats_message_out(int bytes)
{
  pthread_mutex_lock(&stats_mutex);
  allMessagesOutCount++;
  allMessagesOutBytes += bytes;
  
  print_timestamp();
  fprintf(trace_file, "Odchozi zprava - %d bajtu\n", bytes);
  
  pthread_mutex_unlock(&stats_mutex);
  return 0;
}

/* neplatne zpravy*/
int stats_message_invalid()
{
  pthread_mutex_lock(&stats_mutex);
  allMessagesInvalid++;
  print_timestamp();
  fprintf(trace_file, "Prijata neplatna zprava\n.");
  pthread_mutex_unlock(&stats_mutex);
  return 0;
}

/* zapocteni pripojeni */
int stats_connection()
{
  pthread_mutex_lock(&stats_mutex);
  connectionCount++;
  print_timestamp();
  fprintf(trace_file, "Prijato nove pripojeni\n.");
  pthread_mutex_unlock(&stats_mutex);
  return 0;
}

/* zapocteni zacatku hry */
int stats_game_started()
{
  pthread_mutex_lock(&stats_mutex);
  gamesCount++;
  print_timestamp();
  fprintf(trace_file, "Zalozena nova hra.\n");
  pthread_mutex_unlock(&stats_mutex);
  return 0;
}

/* zapocteni konce hry */
int stats_game_finished()
{
  pthread_mutex_lock(&stats_mutex);
  gamesFinished++;
  print_timestamp();
  fprintf(trace_file, "Hra byla ukoncena.\n");
  pthread_mutex_unlock(&stats_mutex);
  return 0;
}

/* zapocteni opusteni hry hracem */
int stats_player_left()
{
  pthread_mutex_lock(&stats_mutex);
  playerLeftCount++;
  print_timestamp();
  fprintf(trace_file, "Hrac opustil hru.\n");
  pthread_mutex_unlock(&stats_mutex);
  return 0;
}

/* zapocteni obnoveni pripojeni hrace */
int stats_player_reconnect()
{
  pthread_mutex_lock(&stats_mutex);
  playerReconnectCount++;
  print_timestamp();
  fprintf(trace_file, "Hrac se pripojil zpet.\n");
  pthread_mutex_unlock(&stats_mutex);
  return 0;
}


