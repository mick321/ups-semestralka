
#include <stdlib.h>
#include <pthread.h>
#include "game.h"
#include "stats.h"

/* ===========   GLOBALNI PROMENNE   ============ */

game_t game[MAX_GAME_COUNT];
pthread_mutex_t game_alloc_mutex;
int game_id_counter = 1000;

/* ===========   PROTOTYPY FUNKCI    ============ */

int check_turn(game_t* game, player_t* player, int x, int y, int output);
int do_turn(game_t* game, player_t* player, int x, int y);

game_t* alloc_game();
int free_game(game_t* game);

/* ===========   FUNKCE              ============ */

/*
  zaslani NOOP - zpravy pro kontrolu, jestli pripojeni stale funguje
*/
int send_noop(player_t* player)
{
    int buf_size = 1;
    char buffer = '\0';

    if (send(player->socket, &buffer, buf_size, 0) == -1)
        return -1;

    stats_message_out(buf_size);
    return 0;
}

/*
  zaslani textove zpravy
*/
int send_text(player_t* player, const char* str)
{
    int buf_size;
    char buffer[1024];

    buf_size = strlen(str) + 1 + 1;

    ((char*)buffer)[0] = MESSAGE_TEXT;
    memcpy(buffer + 1, str, buf_size - 1);

    if (send(player->socket, buffer, buf_size, 0) == -1)
    {
        return -1;
    }

    stats_message_out(buf_size);
    return 0;
}

/*
  zaslani informace o rozlozeni figur atd
*/
int send_game_info(game_t* game, player_t* player)
{
    /* herni struktura + cislo hrace + typ zpravy*/
    int buf_size;
    void* buffer;
    unsigned int temp;

    buf_size = sizeof(game_t) + sizeof(unsigned char) + sizeof(unsigned char);
    buffer = malloc(buf_size);

    ((char*)buffer)[0] = MESSAGE_GAME_STATE;
    temp = htonl(game->game_id);
    memcpy(buffer + 1, &temp, sizeof(unsigned int));
    ((char*)buffer)[5] = player->id;
    temp = htonl(game->turn_number);
    memcpy(buffer + 6, &temp, sizeof(unsigned int));
    ((char*)buffer)[10] = game->state;
    ((char*)buffer)[11] = game->player_on_turn;
    ((char*)buffer)[12] = game->won;
    memcpy(buffer + 13, game->fields, 64 * sizeof(unsigned char));

    if (send(player->socket, buffer, buf_size, 0) == -1)
    {
        free(buffer);
        return -1;
    }

    stats_message_out(buf_size);

    free(buffer);
    return 0;
}

/*
  obsluha pozadavku od hrace
*/
int receive_message(game_t* game, char type, player_t* player)
{
    char tmpx, tmpy;
    int bytesRead;
    switch (type)
    {
    case MESSAGE_NOOP:
        printf("prijato NOOP.\n");
        send_noop(player); /*odpovime*/
        player->last_message = time(NULL);
        stats_message_in(1);
        break;

        /* text message se od clienta neocekava */
        /*
            case MESSAGE_TEXT:
                printf("prijato TEXT.\n");
                break;
        */

    case MESSAGE_TURN:
        printf("prijato TURN.\n");

        if ((bytesRead = recv((*player).socket, &tmpx, sizeof(char), 0)) > 0)
            if ((bytesRead = recv((*player).socket, &tmpy, sizeof(char), 0)) > 0)
            {
                player->last_message = time(NULL);
                stats_message_in(3);
                if (game->left)
                {
                    send_text(player, STR_OPP_NOT_HERE);
                    break;
                }

                printf("Prijat pozadavek na tah na pole [ %d %d ]\n", tmpx + 1, tmpy + 1);
                if (check_turn(game, player, tmpx, tmpy, 1))
                {
                    printf("Tah je validni.\n");
                    do_turn(game, player, tmpx, tmpy);

                    send_game_info(game, &game->player1);
                    send_game_info(game, &game->player2);
                }
                else
                {
                    printf("Tah NENI validni a nebude proveden.\n");
                }

            }

        break;

    default:
        stats_message_invalid();
        printf("prijato NEZNAME.\n");
        send_text(player, STR_MESSAGE_INVALID);
        return 1;
    }
    return 0;
}


/*
  inicializace hry - rozmisteni kamenu, nastaveni hracu
*/
int game_init(game_t* game, int change_player_id)
{
    int onturn_default = change_player_id ? (game->player2.id - 1) : time(NULL) % 2;
    game->player1.id = onturn_default;
    game->player2.id = (game->player1.id + 1) % 2;
    game->player1.id++;
    game->player2.id++;

    game->player1.last_message = time(NULL);
    game->player2.last_message = time(NULL);

    game->players_by_id[0] = NULL;
    game->players_by_id[game->player1.id] = &game->player1;
    game->players_by_id[game->player2.id] = &game->player2;

    game->turn_number = 1;
    game->state = GAME_STATE_RUNNING;
    game->player_on_turn = 1;
    game->won = 0;

    game->left = 0;
    game->reconnect_flag = 0;

    memset(game->fields, 0, sizeof(unsigned char) * 64);

    game->fields[8 * 3 + 3] = 1;
    game->fields[8 * 4 + 4] = 1;
    game->fields[8 * 4 + 3] = 2;
    game->fields[8 * 3 + 4] = 2;

    return 0;
}

/*
  ukonceni hry - spocteni kamenu a nastaveni patricnych priznaku
*/
int finish_game(game_t* game)
{
    int i, j, temp;
    int count[2];

    count[0] = 0;
    count[1] = 0;

    for (i = 0; i < 8; i++)
        for (j = 0; j < 8; j++)
        {
            temp = game->fields[8 * j + i];
            if (temp)
                count[temp-1]++;
        }

    game->state = GAME_STATE_FINISHED;
    if (count[0] > count[1])
        game->won = 1;
    else if (count[0] < count[1])
        game->won = 2;
    else
        game->won = 0;

    if (game->won)
    {
        send_text(&game->player1, game->player1.id == game->won ? STR_YOU_WIN : STR_YOU_LOST);
        send_text(&game->player2, game->player2.id == game->won ? STR_YOU_WIN : STR_YOU_LOST);
    }
    else
    {
        send_text(&game->player1, STR_BOTH_DRAW);
        send_text(&game->player2, STR_BOTH_DRAW);
    }

    return 0;
}

/*
  vykresleni policek - lze vyuzit pro debug
*/
int draw(game_t* game)
{
    int i, j;

    for (j = 0; j < 8; j++)
    {
        printf("  ");
        for (i = 0; i < 8; i++)
        {
            if (game->fields[j * 8 + i] == 0)
                printf("-");
            else
                printf(game->fields[j * 8 + i] == 1 ? "O" : "X");
        }
        printf("\n");
    }

    return 0;
}

/*
  zaslani informaci po obnoveni spojeni
*/
int send_info_after_reconnect(game_t* game)
{
    send_text(&game->player1, STR_RECONNECT_OK);
    send_text(&game->player2, STR_RECONNECT_OK);

    send_game_info(game, &game->player1);
    send_game_info(game, &game->player2);

    return 0;
}

/*
  funkce, tera provede potrebne operace pri odpojeni hrace
*/
int handle_disconnected(game_t* game, int i)
{
    game->state = GAME_STATE_PLAYER_LEFT;
    game->left |= game->players_by_id[i]->id;

    printf("[%d] %s\n", game->game_id, STR_LEFT);
    close(game->players_by_id[i]->socket);
    game->players_by_id[i]->socket = 0;

    stats_player_left();

    /* neodpojili se nahodou oba? */
    if (game->left == (1 | 2))
    {
        printf("[%d] %s\n", game->game_id, STR_WARNING_BOTH_LEFT);
        return 0;
    }

    /* preposlani zpravy o opusteni hry souperi */
    if (i == 1)
        send_game_info(game, game->players_by_id[2]);
    else
        send_game_info(game, game->players_by_id[1]);

    return 0;
}

/*
  funkce, ktera kontroluje vytikani spojeni a usuzuje, jestli ma vubec byt hra jeste
  uschovana v pameti

  vraci 1, ma-li hra byt ukoncena
*/
int check_timeout(game_t* game)
{
    int i;
    time_t now, last_message;

    last_message = (time_t)0;
    now = time(NULL);

    for (i = 1; i <= 2; i++)
    {
        if (game->players_by_id[i]->last_message > last_message)
            last_message = game->players_by_id[i]->last_message;

        if ((game->left & i) == 0)
            if (game->players_by_id[i]->last_message + MAX_PLAYER_WAIT < now)
            {
		printf("[%d] %s\n", game->game_id, STR_PLAYER_TIMEOUT);
                handle_disconnected(game, i);
            }
    }

    if (game->left == (1 | 2))
    {
        if (last_message + MAX_GAME_WAIT < now)
	{
	    printf("[%d] %s\n", game->game_id, STR_GAME_TIMEOUT);
            return 1;
	}
    }

    return 0;
}

/*
  "herni smycka" - vyrizovani tahu (jadro hry!)
  konci ve chvili, kdy nejaky hrac opusti hru nebo hra skonci
*/
#define BUF_SIZE 128
void* game_loop(void* game_voidptr)
{
    int i;         /* pro iterace */
    char m_type;   /* typ prijate zpravy */
    int bytesRead; /* pocet prectenych bajtu ze socketu */

    fd_set fds;    /* sada file descriptoru pro select */
    struct timeval timeout;   /* timeout pro select */
    int smax;                 /* max socket nr pro select */
    int finished;             /* priznak pro skonceni */

    game_t* game;   /* typove spravny pointer na instanci hry */

    /* pretypovani parametru - pthread_create vyzaduje jen (void*) */
    game = (game_t*)game_voidptr;

    /* nastaveni timeoutu */
    timeout.tv_sec = 0;
    timeout.tv_usec = 1000;

    finished = 0;

    /* smycka pro cteni ze socketu */
    while (!finished)
    {
        /* nastaveni "maxima" */
        smax = ((game->player1.socket > game->player2.socket) ? game->player1.socket : game->player2.socket) + 1;

        /* nastaveni file descriptoru */
        FD_ZERO(&fds);

        if ((game->left & game->player1.id) == 0)
            FD_SET(game->player1.socket, &fds);

        if ((game->left & game->player2.id) == 0)
            FD_SET(game->player2.socket, &fds);

        /* iterace nad sockety */
        if (select(smax, &fds, (fd_set *) 0, (fd_set *) 0, &timeout) > 0)
            for (i = 1; i <= 2; i++)
            {
                /* bereme v potaz jen hrace, co je ve hre && poslal nejaka data */
                if ((game->left & i) == 0 && FD_ISSET(game->players_by_id[i]->socket, &fds))
                {
                    printf("[%d] socket hrace %d : ", game->game_id, i);
                    bytesRead = recv(game->players_by_id[i]->socket, &m_type, sizeof(char), 0);

                    /* osetreni chyby (ukonceni spojeni) */
                    if (bytesRead <= 0)
                    {
			handle_disconnected(game, i);                      
		    }
                    /* nedoslo-li k odpojeni hrace, potom dekoduj zpravu */
                    if ((game->left & i) == 0)
                        receive_message(game, m_type, game->players_by_id[i]);
                }
            }

        /* neobnovil nahodou jeden z hracu pripojeni? */
        if (game->reconnect_flag)
        {
            send_info_after_reconnect(game);
            game->reconnect_flag = 0;
        }

        /* setrime cas cpu */
        usleep(1000);

        finished = check_timeout(game);

        /* nekdo vyhral? bajecne, konec! */
        if (game->won)
            finished = 1;
    }

    /* uzavreni socketu */
    if (game->player1.socket)
        close(game->player1.socket);
    if (game->player2.socket)
        close(game->player2.socket);

    /* leavnuli oba? tak to aspon hezky rekneme */
    if (game->left == (1 | 2))
    {
        printf("[%d] %s\n", game->game_id, STR_BOTH_LEFT);
    }
    else
    {
        stats_game_finished();
    }

    /* uvolneni hry */
    free_game(game);
    printf("[%d] %s\n", game->game_id, STR_GAME_EXIT);
    return 0;
}

/*
  start hry
*/
int start_game(int sock1, struct sockaddr_in sockaddr1, int sock2, struct sockaddr_in sockaddr2)
{
    int rtn;
    game_t* game;
    pthread_t thread;

    /* vyzadame si hru...*/
    game = alloc_game();
    if (!game)
        return 1;

    /* nastavime sockety */
    game->player1.socket = sock1;
    game->player2.socket = sock2;

    /* spustime obecnou inicializaci */
    game_init(game, 0);

    /* zasleme pocatecni zpravy */
    send_game_info(game, &game->player1);
    send_game_info(game, &game->player2);

    send_text(game->players_by_id[1], STR_MESSAGE_WHITE);
    send_text(game->players_by_id[2], STR_MESSAGE_BLACK);

    printf("[%d] %s\n", game->game_id, INFO_GAME_CREATED);
    /* rozbehneme vlakno */
    rtn = pthread_create(&thread, NULL, game_loop, (void*)game);

    return rtn;
}

/*
  Bezpecne cteni herniho pole, pokud sahneme mimo, je vracena -1.
*/
int safe_read_field(game_t* game, int x, int y)
{
    if (x < 0 || x > 7 || y < 0 || y > 7)
        return -1;

    return game->fields[y * 8 + x];
}

/*
  Vyhledavani policka s id "id" ve smeru sx, sy od policka x, y (vcetne).
  Vraci 1 ("true") pri nalezeni.
*/
int safe_find_dir(game_t* game, int x, int y, int id, int sx, int sy)
{
    int chk;

    while (1)
    {
        chk = safe_read_field(game, x, y);

        x += sx;
        y += sy;

        if (chk <= 0)
            return 0;
        else if (chk == id)
            return 1;
    }
}

/*
  Zkontroluje dany tah.
*/
int check_turn(game_t* game, player_t* player, int x, int y, int output)
{
    int i, j, chk;
    char buffer[128];
    int other_one = (*player).id == 1 ? 2 : 1;

    if ((*player).id != game->player_on_turn)
    {
        if (output)
        {
            printf(STR_NOT_ON_TURN, (*player).id);
            send_text(player, STR_OPP_TURN);
        }
        return 0;
    }

    /* kontrola, jestli je policko prazdne */
    if (safe_read_field(game, x, y) != 0)
    {
        if (output)
        {
            sprintf(buffer, STR_FIELD_OCC, x + 1, y + 1);
            printf("%s\n", buffer);
            send_text(player, buffer);
        }
        return 0;
    }

    /* kontrola "obkliceni" souperova kamene */
    for (i = x - 1; i <= x + 1; i++)
        for (j = y - 1; j <= y + 1; j++)
        {
            if (i == x && j == y)
                continue;

            chk = safe_read_field(game, i, j);
            if (chk == other_one) /* toto je nadejne */
            {
                chk = safe_find_dir(game, i, j, (*player).id, i-x, j-y);
                if (chk)
                    return 1; /* nasli jsme! */
            }
        }
    if (output)
        printf(STR_TURN_INVALID);
    return 0;
}

/*
  Provede dany tah. Nekontroluje se spravnost tahu, predem je nutne zavolat check_turn!
*/
int do_turn(game_t* game, player_t* player, int x, int y)
{
    int i, j, tmpx, tmpy;
    int turn_possible;
    int try_count;

    // otoceni obklicenych souperovych kamenu
    for (i = -1; i <= 1; i++)
        for (j = -1; j <= 1; j++)
            if (i != 0 || j != 0)  // iterace pres vsech osm smeru
            {
                if (safe_find_dir(game, x + i, y + j, (*player).id, i, j))
                    // v hledanem smeru musi lezet muj kamen
                {
                    tmpx = x + i;
                    tmpy = y + j;
                    // prebarveni kamenu
                    while (safe_read_field(game, tmpx, tmpy) > 0 &&
                            safe_read_field(game, tmpx, tmpy) != (*player).id)
                    {
                        game->fields[tmpy * 8 + tmpx] = (*player).id;
                        tmpx += i;
                        tmpy += j;
                    }
                }
            }

    // obsazeni pole
    game->fields[y * 8 + x] = (*player).id;

    try_count = 0;
    turn_possible = 0;

    /* kontrola, jestli vubec ma dalsi hrac cim tahnout...
       pokud ne, zkousime protivnika... pokud ani ten, hra konci */
    do
    {
        game->player_on_turn++;
        if (game->player_on_turn > 2)
            game->player_on_turn = 1;

        for (i = 0; i < 8; i++)
            for (j = 0; j < 8; j++)
            {
                if (check_turn(game, game->players_by_id[game->player_on_turn], i, j, 0))
                {
                    turn_possible = 1;
                }
            }

        try_count++;

    } while(try_count < 2 && !turn_possible);

    if (!turn_possible)
        finish_game(game); /* nikdo nemuze tahnout, hra konci */

    /* kdybychom nahodou chteli debugovat */
    /*draw(game);*/

    return 0;
}

/*
  Nalezeni konkretni hry podle id. Pokud dana hra neexistuje, vrati NULL.
*/
game_t* find_game(int game_id)
{
    int i;

    for (i = 0; i < MAX_GAME_COUNT; i++)
        if (game[i].allocated && game[i].game_id == game_id)
            return game + i;

    return NULL;
}

/*
  Obnoveni pripojeni do hry.
*/
int reconnect_game(int game_id, int player_id, int sock, struct sockaddr_in sockaddr)
{
    game_t* game;
    player_t* player;

    game = find_game(game_id);
    /* kontrola chyb */
    if (game == NULL || game->allocated == 0 || game->left == 0 || (game->left & player_id) == 0)
    {
        player_t temp_player;
        temp_player.socket = sock;
        temp_player.id = 0;
        send_text(&temp_player, STR_RECONNECT_FAIL);
        return -1;
    }

    player = game->players_by_id[player_id];

    game->left -= player_id;
    game->state = game->left ? GAME_STATE_PLAYER_LEFT : GAME_STATE_RUNNING;
    player->socket = sock;
    player->last_message = time(NULL);

    game->reconnect_flag = 1; /* nastaveni priznaku, herni vlakno zpracuje zbytek */

    stats_player_reconnect();

    return 0;
}

/*
  Inicializace pole her.
*/
int init_game_array()
{
    memset(game, 0, sizeof(game_t) * MAX_GAME_COUNT);
    pthread_mutex_init(&game_alloc_mutex, NULL);
    return 0;
}

/*
  Alokace jedne hry (prideleni pameti)
*/
game_t* alloc_game()
{
    int i;
    game_t* rtn;
    rtn = NULL;
    pthread_mutex_lock(&game_alloc_mutex);

    for (i = 0; i < MAX_GAME_COUNT; i++)
        if (game[i].allocated == 0)
        {
            rtn = game + i;
            rtn->game_id = game_id_counter;
            rtn->allocated = 1;
            game_id_counter++;
            break;
        }

    pthread_mutex_unlock(&game_alloc_mutex);
    return rtn;
}

/*
  Dealokace jedne hry
*/
int free_game(game_t* game)
{
    pthread_mutex_lock(&game_alloc_mutex);

    game->allocated = 0;

    pthread_mutex_unlock(&game_alloc_mutex);
    return 0;
}
