#ifndef __stats_h
#define __stats_h

/* inicializace statistik */
int stats_init();
/* vytisknuti statistik */ 
int stats_print();
/* zapocteni prichozi zpravy */
int stats_message_in(int bytes);
/* zapocteni odchozi zpravy */
int stats_message_out(int bytes);
/* zapocteni neplatne prichozi zpravy */
int stats_message_invalid();
/* zapocteni pripojeni */
int stats_connection();
/* zapocteni zacatku hry */
int stats_game_started();
/* zapocteni konce hry */
int stats_game_finished();
/* zapocteni opusteni hry hracem */
int stats_player_left();
/* zapocteni obnoveni pripojeni hrace */
int stats_player_reconnect();

#endif
