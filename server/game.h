#ifndef __game_h
#define __game_h


#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <errno.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <time.h>

/* ======================= struktury ========================= */

/*
  struktura hrace
*/
typedef struct PLAYER
{
    int socket;
    unsigned char id; /* 1 = bily, 2 = cerny */
    time_t last_message;
} player_t;

/*
  struktura uchovavajici stav hry
*/
typedef struct GAME
{
    int game_id;
    int turn_number;
    unsigned char state;
    unsigned char player_on_turn;
    unsigned char won;
    unsigned char fields[64];
    player_t player1, player2;
    player_t* players_by_id[3];

    int allocated;
    int left;

    int reconnect_flag;
} game_t;

/* ======================= funkce ========================= */

/*
  Provoz hry nad dvema sockety.

  return: 0 pri normalnim prubehu a dokonceni komunikace (hry).
          1 pri pretizeni serveru
*/
int start_game(int sock1, struct sockaddr_in sockaddr1, int sock2, struct sockaddr_in sockaddr2);

/*
  Nalezeni konkretni hry podle id. Pokud dana hra neexistuje, vrati NULL.
*/
game_t* find_game(int game_id);

/*
  Opetovne pripojeni do hry. Pokud se vydari, vrati 0.
*/
int reconnect_game(int game_id, int player_id, int sock, struct sockaddr_in sockaddr);

/*
  Inicializace pole her.
*/
int init_game_array();


/* ======================= konstanty ========================= */

#define MAX_GAME_COUNT 1024
/* cekani na hrace - keep alive timeout */
#define MAX_PLAYER_WAIT 30
/* timeout v pripade obou odpojenych - doba uschovani hry v pameti */
#define MAX_GAME_WAIT 300

#define GAME_STATE_RUNNING      0
#define GAME_STATE_FINISHED     1
#define GAME_STATE_PLAYER_LEFT  2

#define MESSAGE_NOOP 0
#define MESSAGE_TEXT 1
#define MESSAGE_GAME_STATE 2
#define MESSAGE_TURN 3

#define INFO_GAME_CREATED "Hra byla vytvorena."

#define STR_MESSAGE_INVALID "Zprava vyslana na server je neplatna!"
#define STR_GAME_EXIT "Hra byla ukoncena!"
#define STR_GAME_TIMEOUT "Vyprsel timeout pro pripojeni hracu zpet do opustene hry."
#define STR_PLAYER_TIMEOUT "Vyprsel timeout u hrace, nejspise je odpojen."
#define STR_WARNING_BOTH_LEFT "Oba hraci opustili hru, pokud se nepripoji vcas zpet, bude hra ukoncena."
#define STR_BOTH_LEFT "Oba hraci opustili hru, hra byla ukoncena!"
#define STR_RECONNECT_FAIL "Pokus o obnoveni spojeni se nezdaril."
#define STR_RECONNECT_OK  "Hrac se vratil do hry!"
#define STR_LEFT  "Hrac se odpojil!"

#define STR_MESSAGE_WHITE "Hrajete za bile kameny."
#define STR_MESSAGE_BLACK "Hrajete za cerne kameny."
#define STR_OPP_TURN "Na tahu je souper!"

#define STR_OPP_NOT_HERE "Ceka se na opetovne pripojeni soupere..."
#define STR_OPP_LEFT "Spojeni se souperem bylo ztraceno. Pockejte, prosim..."

#define STR_NOT_ON_TURN "Hrac %d neni na tahu.\n"
#define STR_FIELD_OCC "Pole %d %d je jiz obsazene."
#define STR_TURN_INVALID "Kamen neobklicuje souperovy kameny.\n"

#define STR_YOU_LOST "Bohuzel jste prohral."
#define STR_YOU_WIN  "Gratuluji, zvitezil jste!"
#define STR_BOTH_DRAW  "Remiza!"

#endif
