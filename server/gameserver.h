#ifndef __gameserver_h
#define __gameserver_h
 
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <signal.h>

#define ERR_CANT_CREATE_SOCKET "Nepodarilo se vytvorit socket serveru."
#define ERR_CANT_BIND_SOCKET "Nepodarilo se svazat socket s lokalni adresou."
#define ERR_CANT_LISTEN "Nepodarilo se naslouchat."
#define ERR_CANT_SET_SOCKOPT "Varovani: nepodarilo se nastavit keepalive pro spojeni."
#define ERR_GAME_NOT_CREATED "Nepodarilo se vytvorit hru."

#define INFO_WELCOME "Server pro hru reversi\nMichal Zak 2012\n"
#define INFO_END "Beh serveru byl ukoncen."
#define INFO_PLAYER_CONNECTED "Pripojil se hrac z adresy"
#define INFO_AWAITING "Cekam na dvojici hracu..."
#define INFO_PLAYER_ONE_LEFT "Prvni hrac se odpojil, je nahrazen nove prichozim."
#define INFO_PLAYER_RECONNECTED_TO "Hrac se znovu pripojil do hry "


#endif
