#include "gameserver.h"
#include "game.h"
#include "stats.h"

#define SOCKET_ERROR -1
#define MY_PORT 8000


int s;
struct sockaddr_in sa;

struct sockaddr_in waiting_addr;
int waiting_socket;
int has_waiting = 0;

/*
  Vytvori serverovy socket a svaze ho s lokalni adresou.

  return
	 0	uspech
	-1	chyba
*/
int create_bind_listen_socket()
{
    int optval;

    // Vytvorime stream socket
    s = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (s == SOCKET_ERROR)
    {
        printf("%s (%3d)\n", ERR_CANT_CREATE_SOCKET, errno);
        return -1;
    }

    optval = 1;
    setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(int));

    // svazani socketu s lokalni adresou
    sa.sin_family = AF_INET;
    sa.sin_port = htons(MY_PORT);
    sa.sin_addr.s_addr = INADDR_ANY;
    if (bind(s, (struct sockaddr*)&sa, sizeof(sa)))
    {
        printf("%s (%3d)\n", ERR_CANT_BIND_SOCKET, errno);
        close(s);
        return -1;
    }

    if (listen(s, 1))
    {
        printf("%s (%4d)\n", ERR_CANT_LISTEN, errno);
        close(s);
        return -1;
    }

    return 0;
}

/*
  Zkontrolovani daneho socketu, jestli nahodou nebylo spojeni jiz preruseno.
*/
int check_connection(int socket, int readgameid)
{
    fd_set fds;
    struct timeval tv;
    int selrtn;
    int bytesRead;

    FD_ZERO(&fds);
    FD_SET(socket, &fds);

    tv.tv_sec = 1;
    tv.tv_usec = 0;
    selrtn = select(socket + 1, &fds, NULL, NULL, &tv);
    if (selrtn < 0)
        return -1;

    if (selrtn > 0)
    {
        int bytesToRead;
        ioctl(socket, FIONREAD, (char*)&bytesToRead);

        if (bytesToRead == 0)
            return -1;

	if (readgameid)
	{
	    int game_id;

            if ((bytesRead = recv(socket, &game_id, sizeof(int), 0)) > 0)
	    {
		return ntohl(game_id);
	    }

	    return -1;
	}
    }

    return 0;
}

/*
  Prijeti dvou klientu (hracu) a jejich predani do podprocesu hry (partie).
*/
int accept_player()
{
    int socket;
    struct sockaddr_in socket_address;
    unsigned int ain_size;

    int result;

    /*
    int optval, optsize;
    */

    ain_size = sizeof(struct sockaddr_in);

    if ((socket=accept(s, (struct sockaddr*) &socket_address, &ain_size)) == SOCKET_ERROR)
        return -1;

    /* zakomentovano, rpotoze se to musi resit jinak (mensi tmeout jak dve hodiny neni standardni */ 
    /*
    optval = 1;
    optsize = sizeof(int);
    result = 0;
    if (setsockopt(socket, SOL_SOCKET, SO_KEEPALIVE, &optval, optsize) == 0)
    {
	int keepcnt = 3;
	int keeptime = 15;
	int keepintvl = 2;
        if (setsockopt(socket, SOL_TCP, TCP_KEEPCNT, &keepcnt, optsize) == 0 &&
            setsockopt(socket, SOL_TCP, TCP_KEEPIDLE, &keeptime, optsize) == 0 &&
            setsockopt(socket, SOL_TCP, TCP_KEEPINTVL, &keepintvl, optsize) == 0)
	{	 
            result = 1;
	}
    }
    
    if (result == 0)
    {
	printf("%s\n", ERR_CANT_SET_SOCKOPT);
    }
    */

    stats_connection();

    printf("%s %s.\n", INFO_PLAYER_CONNECTED, inet_ntoa(socket_address.sin_addr));

    result = check_connection(socket, 1);

    if (result == -1)
    {
	/* chyba */
        close(socket);
        return -1;
    }
    else if (result == 0)
    {
	/* matchmaking */
        if (has_waiting)
        {
            if (check_connection(waiting_socket, 0) == 0)
            {
                has_waiting = 0;
                if (start_game(socket, socket_address, waiting_socket, waiting_addr) == 0)
                { 
		    /* vse ok */
		    stats_game_started();
                }
                else
                {
                    printf("%s\n", ERR_GAME_NOT_CREATED);
                }
            }
            else
            {
		close(waiting_socket);
                waiting_addr = socket_address;
                waiting_socket = socket;
                printf("%s\n", INFO_PLAYER_ONE_LEFT);
            }
        }
        else
        {
            has_waiting = 1;
            waiting_addr = socket_address;
            waiting_socket = socket;
        }
    }
    else
    {
	/* reconnect */
        int player_id = 0; 
	int bytesRead;
        
        if ((bytesRead = recv(socket, &player_id, sizeof(int), 0)) > 0)
	{
	    player_id = ntohl(player_id);
	}

	if (reconnect_game(result, player_id, socket, socket_address) != 0)
	{
	    printf("%s \n", STR_RECONNECT_FAIL);
	    close(socket);
	    return -1;
	}
	else
	{
            printf("%s [%d].\n", INFO_PLAYER_RECONNECTED_TO, result);
	}
    }

    return 0;
}

/*
  osetreni ctrl+c
*/
void signal_callback_handler(int signum)
{
    if (s)
	close(s);

    printf("\n");
    printf("%s\n", INFO_END);
    printf("\n");

    stats_print();

    exit(signum);
}

/*
  Vstupni funkce programu.
*/
int main(int argc, char* argv[])
{
    signal(SIGINT, signal_callback_handler);
    stats_init();

    printf("%s\n", INFO_WELCOME);
    init_game_array();

    if (create_bind_listen_socket() < 0)
        return -1;

    while(1)
    {
        printf("%s\n", INFO_AWAITING);
        if (accept_player() < 0)
            continue;  /* chyba na teto urovni by nemela shodit jiz bezici servery! */
    }

    /* unreachable code, nicmene v budoucnu muze byt nekdy server ukoncen jinak nez natvrdo CTRL + C*/

    return 0;
}

